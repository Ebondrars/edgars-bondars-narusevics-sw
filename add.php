<!DOCTYPE html>
<html>
	
<?php include('templates/header.php'); ?>
<?php include('additem.php'); ?>
	<section class="container grey-text ">
		<h4 class="text-center pt-2 pb-3 ">Add Product</h4>
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
			<label>SKU</label>
			<input type="text" name="SKU" value="<?php echo htmlspecialchars($Name) ?>">
			<div class="text-danger"><?php echo $errors['SKU']; ?></div>
			<label>Name</label>
			<input type="text" name="Name" value="<?php echo htmlspecialchars($Name) ?>">
			<div class="text-danger"><?php echo $errors['Name']; ?></div>
			<label>Price</label>
			<input type="number" name="Price" step="0.01" value="<?php echo htmlspecialchars($Price) ?>">
			<div class="text-danger"><?php echo $errors['Price']; ?></div>
			<label >Chose item Type</label>	
				<div class = "center">
					<select name = "itemdropdown">
					<label>Chose item type </label>
 					<option value="" selected disabled hidden >Choose here</option>
 					<option value ='1' >book</option>
	 				<option value ='2' >CD</option>
	 				<option value ='3' >furniture</option>
					</select> 
					<div class="text-danger"><?php echo $errors['itemdropdown']; ?></div>
					</div>	
					<div id="dropdown_fields"></div>
					<!-- book textfields and inputs -->
					<div id="option1_text" ><b>Please provide book weight in KG</b> <br>
					<label>Weight in KG</label><br>
					<input type="number" step="0.01" name="Weight"?>
					<div class="text-danger"><?php echo $errors['Weight']; ?></div>
					</div>
					<!-- CD textfields and inputs -->
					<div id="option2_text" ><b>Write down CD memory size in MB</b><br>
					<label>Size in MB</label><br>
					<input type="number" step="0.01" name="Size"?>
					<div class="text-danger"><?php echo $errors['Size']; ?></div>
					</div>
					<!-- Furniture textfields and inputs -->
					<div id="option3_text"><b>Provide dimensions in HxWxL</b><br>
					<label>Height</label><br>
					<input type="number" name="Height"?>
					<div class="text-danger"><?php echo $errors['Height']; ?></div>
					<label>Width</label><br>
					<input type="number" name="Width"?>
					<div class="text-danger"><?php echo $errors['Width']; ?></div>
					<label>Lenght</label><br>
					<input type="number" name="Lenght"?>
					<div class="text-danger"><?php echo $errors['Lenght']; ?></div>
				</div>
			<div class="center pt-3">
				<input type="submit" name="submit" value="Submit" class="btn brand z-depth-0">
			</div>
		</form>
	</section>
</html>



<script type="text/javascript">

$(document).ready(function () {
	//Hides all diferent dropdown options
     var op1 =  $('#option1_text').detach();
     var op2 =  $('#option2_text').detach();
     var op3 =  $('#option3_text').detach();
    
$('select[name="itemdropdown"]').change(function(){

//When corresponding dropdown value is selected correct form is shown
    if ($(this).val() == "1")
    {
    	$('#dropdown_fields').append(op1);
    	$('#option_text').detach();
		$('#option3_text').detach();
    }
        if ($(this).val() == "2")
    {
        $('#dropdown_fields').append(op2);
        $('#option1_text').detach();
        $('#option3_text').detach();
    }
        if ($(this).val() == "3")
    {
        $('#dropdown_fields').append(op3);
        $('#option1_text').detach();
     	$('#option2_text').detach();
    }
    
})});
</script>