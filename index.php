<?php 

$connection = mysqli_connect('localhost', 'test','test','product_db');

if(!$connection){
	echo 'Connection error: ' . mysql_connect_error();
}
// write querry for all items
$sql = 'SELECT SKU, Name, Price,type_id, Description  FROM items';
//make querry get ressult 
$result = mysqli_query($connection, $sql);
//fetch the resulting rows as an array 
$items = mysqli_fetch_all($result, MYSQLI_ASSOC);
//free result from memory 
mysqli_free_result($result);
//close connection
mysqli_close($connection);

?>

<!DOCTYPE html>
<html>
<?php include('templates/header.php'); ?>
	<form method="post" action = "delete.php">	
		<div class="containter content">
			<h2 class="text-center">Items</h2>
				<div class= "card-deck text-center row">
				<!-- for each item create card -->
				<?php foreach($items as $item) { ?>
					<div class="col-sm-3 pb-3 ">
					<div class="card bg-white">
						<div class="card-body">
							<!-- make checkbox with value of SKU for mass delete function -->
   							<input class="float-left" type ='checkbox' name = 'checkbox[]' value =<?php echo $item['SKU'];?> >
							<!-- Fill cards with information -->
							<span class="card-text"><b><?php echo htmlspecialchars($item['SKU'])  ?></b></span>
							<div class="card-text"><?php echo htmlspecialchars($item['Name']) ?></div>
							<div class="card-text"><?php echo htmlspecialchars($item['Price']). ' $ ' ?></div>
							<div class="card-text">
							<!-- based ont item type add unit of measurement -->
							<?php switch ($item['type_id']) { 
    						case 1:
        					echo htmlspecialchars($item['Description']). ' kg ';
        					break;
    						case 2:
        					echo htmlspecialchars($item['Description']). ' MB ' ;
        					break;
    						case 3:
        					echo htmlspecialchars($item['Description']);
        					break;}?>
							</div>
						</div>
					</div>
					</div>
					<?php } ?>
				</div>
				<div class="text-center pt-5">
				<input class = " text-white brand" type="submit" name="delete" id="delete" value="Mass Delete">
				</div>
		</div>
	</form>
</html>